
$(document).ready(function (event) {
    $( ".menu-parent > li" ).hover(function () {
        $( ".menu-parent > li" ).removeClass('active')
        $(this).addClass('active')
    });

    $(".view-menu-mb").click(function () {
        $(this).toggleClass("change-icon-mb");
        $(".nav-block").toggleClass("open-mb");


    });
    if(innerWidth < 576){
        $('.menu-child').hide();
        $(".has-child > a").click(function () {
            $('.menu-child').slideUp();
            if (!$(this).next(".menu-child").is(":visible")) {
                $(this).next(".menu-child").slideDown();
            }
        });
    }
    
    $('.datepicker').datepicker({
        language: "vi",
        autoclose: true,
        format: "dd/mm/yyyy"
      });

      $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
		var input_label = $(this).closest('.input-group').find('.file-input-label'),
			log = numFiles > 1 ? numFiles + ' files selected' : label;

		if( input_label.length ) {
			input_label.text(log);
		} else {
			if( log ) alert(log);
		}
	});
});
$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });